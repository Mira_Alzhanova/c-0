#include "../include/box.h"

bool cargo::Box::operator==(const Box &box) const {
    return length == box.length &&
           width == box.width &&
           height == box.height &&
           weight == box.weight &&
           value == box.value;
}

bool cargo::Box::operator!=(const cargo::Box &box) const {
    return !(box == *this);
}

int cargo::Box::getLength() const {
    return length;
}

void cargo::Box::setLength(int length) {
    Box::length = length;
}

int cargo::Box::getWidth() const {
    return width;
}

void cargo::Box::setWidth(int width) {
    Box::width = width;
}

int cargo::Box::getHeight() const {
    return height;
}

void cargo::Box::setHeight(int height) {
    Box::height = height;
}

double cargo::Box::getWeight() const {
    return weight;
}

void cargo::Box::setWeight(double weight) {
    Box::weight = weight;
}

int cargo::Box::getValue() const {
    return value;
}

void cargo::Box::setValue(int value) {
    Box::value = value;
}

int cargo::Box::totalValue(cargo::Box *box, int boxLen) {
    int totalValue = 0;
    for (int i = 0; i < boxLen; i++) {
        totalValue += box[i].value;
    }
    return totalValue;
}

bool cargo::Box::isSumLWHLessThan(cargo::Box *box, int boxLen, int value) {
    for (int i = 0; i < boxLen; i++) {
        if (box[i].length + box[i].width + box[i].height > value) {
            return false;
        }
    }
    return true;
}

double cargo::Box::getMaxWeightUnderVolume(cargo::Box *box, int boxLen, int maxV) {
    double maxWeigth = 0.0;
    for (int i = 0; i < boxLen; i++) {
        if (box[i].length * box[i].width * box[i].height < maxV) {
            if (box[i].weight > maxWeigth) {
                maxWeigth = box[i].weight;
            }
        }
    }
    return maxWeigth;
}

cargo::Box::Box(int length, int width, int height, double weight, int value) : length(length), width(width),
                                                                               height(height), weight(weight),
                                                                               value(value) {}
















