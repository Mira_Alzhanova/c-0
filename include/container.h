#pragma once

#include "box.h"
#include "vector"

namespace cargo{
    class Container{
        std::vector<Box> boxes;
        int length;
        int width;
        int height;
        double weightLimit;

    public:
        Container(int length, int width, int height, double weightLimit);

        unsigned long long addBox(Box box);

        void eraseBox(int index);

        int getAmountOfBoxes();

        Box getBox(int i);

        double getBoxesWeightSum();

        double getBoxesValuetSum();

        friend std::ostream &operator<<(std::ostream &os, const Container &container){
            os << " length: " << container.length << " width: " << container.width
               << " height: " << container.height << " weightLimit: " << container.weightLimit << "\n" << "boxes: ";
            for(Box b: container.boxes){
                os << b;
            }
            return os;
        }

        friend std::istream &operator>>(std::istream &is, Container &container) {
            is >> container.length >> container.width >> container.height >> container.weightLimit;
            for(int i = 0; i < container.boxes.size(); i++){
                is >> container.boxes[i];
            }
            return is;
        }

        Box operator[](int i);
    };
}
